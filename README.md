# Aperture - Game Engine

[![works badge](https://cdn.jsdelivr.net/gh/nikku/works-on-my-machine@v0.2.0/badge.svg)](https://github.com/STARRY-S/Aperture)

> Current status: Developing...

Aperture: A simple & tiny game engine based on OpenGL ES,
just for learning OpenGL and render stuff, for fun.

Current status (TODOs)
----

- [x] Texture load
- [x] Model import
- [x] Lightning stimulation
    - [x] Directional light
    - [x] Point lights
    - [x] Spot lights
    - [x] Diffuse texture
    - [x] Specular texture
    - [ ] Normal texture
- [ ] Cross platform:
    - [x] Windows (Need update, out of date)
    - [x] Android (see [Aperture-Android](https://github.com/STARRY-S/GameEngine-Android)) (Need update, out of date)
    - [x] Linux
    - [ ] ~~Mac OS~~ (I will add it when I have a Mac)
- [x] Music & Sound engine
- [x] Font rendering
- [ ] Network
- [ ] Physic engine (WIP)
    - [ ] Collision detection (WIP)
    - [x] Gravaty
    - [x] Jumping
    - [ ] hit
- [ ] GUI tools (User graphics)
    - [x] Orthographic shader program
    - [x] Font rendering
- [ ] Database

## Demo

Minecraft model:
![](images/demo-mc-2.png)

![](images/demo-mc-single-color.png)

Backpack model:
![](images/demo-backpack.png)

## Usage

### Linux

```
$ git clone https://github.com/STARRY-S/Aperture.git && cd Aperture
$ mkdir build && cd build
$ cmake .. && make -j4
```

### Android

See [Aperture-Android](https://github.com/STARRY-S/Aperture-Android)

### Windows

1. Install [MSYS2](https://www.msys2.org/).
2. Install mingw64 build dependencies.
   ```
   $ pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-cmake \
         mingw-w64-x86_64-mesa     mingw-w64-x86_64-assimp \
         mingw-w64-x86_64-cglm     mingw-w64-x86_64-glfw   \
         mingw-w64-x86_64-ninja    mingw-w64-x86_64-openal \
         mingw-w64-x86_64-freealut mingw-w64-x86_64-ffmpeg
   ```
3. Update the system enviroment, add the `msys64/mingw64/bin` folder to `PATH` variable.
   ![](images/env.png)
4. Clone the source code of this repository, open it in windows powershell,
   ```
   > cd \path\to\the\source\code\
   > mkdir build
   > cd build
   > cmake ..
   > ninja
   ```
5. You can use `cmake -DCMAKE_C_COMPILER=gcc` to specify a C compiler.

## Dependencies

- OpenGL ES 3.0
- cglm
- Assimp
- stb_image
- GLAD
- GLFW
- ffmpeg
- openAL
- freeALUT

The Minecraft OBJ file is generated by [jMc2Obj](https://github.com/jmc2obj/j-mc-2-obj);

Font: https://www.fontspace.com/roboto-remix-font-f26577

## License

The source code of this project is under [Apache 2.0](LICENSE) license.
